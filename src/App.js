
import Characters from "./components/Characters";
import { useState, useEffect } from 'react';

function App() {
    const [characterList, setCharacterList] = useState([]);
    let [page, setPage] = useState(1);
    let tra = `https://rickandmortyapi.com/api/character/?page=${page}`;
    useEffect(() => {
        fetch(tra)
        .then(response => response.json())
        .then(response => setCharacterList(response.results))
        .catch(error => console.log(error))
    }, [tra])
  
    return (
        <div>
            <Characters lista={characterList} setPage={setPage} page={page}/>
       
        </div>
       
    )
	
}

export default App;
