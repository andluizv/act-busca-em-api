import CharCard from "../CharCard";
import "./styles.css"

const Characters = ({ lista, setPage, page }) => {
	
    return (
		<div>
			<h1>Meus personagens</h1>
			<div className={"algo"}>
				<div
					className={"arrows"}
					onClick={() => setPage(page > 1 ? page = page - 1 : page = 1)}
				> &laquo;
				</div>
				<div className={"list"}>
					{lista.map((item) => (
						<CharCard personagem={item} />
					))}
				</div>
				<div
					className={"arrows"}
					onClick={() => setPage((page = page + 1))}
				>
					{" "}
					&raquo;{" "}
				</div>
			</div>
		</div>
	);
};

export default Characters;
