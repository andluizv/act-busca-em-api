import "./styles.css"

const CharCard = ({ personagem }) => {

    return (
		<div key={personagem.id} className={personagem.status + ' char-div'}>
			<h3>{personagem.name}</h3>
			<img src={personagem.image} alt={`Foto de ${personagem.name}`} />
			<p>
				Espécie:{" "}
				{personagem.species}
			</p>
			<span>
				Origem:{" "}
				<a href={personagem.origin.url}>{personagem.origin.name}</a>
			</span>
			<h4 className={personagem.status + '-destaq'}>
				Status:{" "}
				{personagem.status === "Alive"
					? "Alive"
					: personagem.status === "Dead"
					? "Dead"
					: "Unknown"}
			</h4>
		</div>
	);
}

export default CharCard;